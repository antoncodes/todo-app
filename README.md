# AntonCodes - Todo App

A demo application repository for the AntonCodes channel:

https://www.youtube.com/channel/UCB6hH4uyI3AigJkTNoQ_bcg

## Installation

1. Clone the repository (via HTTPS or SSH):

```bash
git clone https://gitlab.com/antoncodes/todo-app.git # HTTPS
# or
git clone git@gitlab.com:antoncodes/todo-app.git # SSH
```

2. Switch to the project root:

```bash
cd todo-app
```

3. Install dependencies:

```bash
npm install
```

4. Set up the pre-commit hook:

```bash
npm run prepare
```

5. **Put a star to the repository ;)**
