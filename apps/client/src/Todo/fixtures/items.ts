import TodoItem from 'Todo/interfaces/item';

const todoItemsFixture: TodoItem[] = [
  {
    label: 'Display todos',
    done: true,
  },
  {
    label: 'Toggle a todo',
    done: false,
  },
  {
    label: 'Delete a todo',
    done: false,
  },
  {
    label: 'Create a todo',
    done: false,
  },
];

export default todoItemsFixture;
