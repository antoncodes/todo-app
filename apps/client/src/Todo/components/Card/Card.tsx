import { Checkbox, FormControlLabel } from '@mui/material';
import TodoItem from 'Todo/interfaces/item';

export default function TodoCard({ label, done }: TodoItem) {
  return (
    <li>
      <FormControlLabel
        control={<Checkbox defaultChecked={done} />}
        label={label}
      />
    </li>
  );
}
