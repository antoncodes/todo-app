import { Box, CircularProgress, Typography } from '@mui/material';
import useTodoItems from 'Todo/queries/use-items';
import TodoCard from '../Card';

export default function TodoMain() {
  const { loading, error, data: todos } = useTodoItems();

  if (loading) {
    return (
      <Box textAlign="center" pt={4}>
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return (
      <Box textAlign="center" pt={4}>
        <Typography color="error">An error occurred.</Typography>
      </Box>
    );
  }

  return (
    <>
      <Typography variant="h2" component="h1">
        My todos
      </Typography>
      {todos && todos.length > 0 ? (
        <ul style={{ listStyle: 'none', padding: 0 }}>
          {todos.map((item, index) => (
            <TodoCard key={index} {...item} />
          ))}
        </ul>
      ) : (
        <Box pt={4}>
          <Typography color="textSecondary">No todos yet.</Typography>
        </Box>
      )}
    </>
  );
}
