export default interface TodoItem {
  label: string;
  done: boolean;
}
