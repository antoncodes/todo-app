import { Box, Container } from '@mui/material';
import CommonNavbar from 'Common/components/Navbar';
import TodoMain from 'Todo';

export default function App() {
  return (
    <div>
      <CommonNavbar />
      <Box mb={{ xs: 5, md: 6 }} />
      <Container component="main">
        <TodoMain />
      </Container>
    </div>
  );
}
