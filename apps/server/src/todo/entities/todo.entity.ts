export interface TodoEntity {
  label: string;
  done: boolean;
}
