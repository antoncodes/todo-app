import { TodoEntity } from '../entities/todo.entity';

export const todosFixture: TodoEntity[] = [
  {
    label: 'Initialize NestJS server',
    done: true,
  },
  {
    label: 'Generate Todo REST API',
    done: true,
  },
  {
    label: 'Connect to database',
    done: false,
  },
];
